# CliTools

This library provides helper classes to make working with the command line interface easier. Right now, the following
features are available:

- [`CommandLine`](https://github.com/Syroot/CliTools/wiki/CommandLine): Simply instantiate a child class of `CommandLine` to assign (parsed) command line arguments to fields and properties when instantiating it.

## Installation

The library is available in the [Syroot.CliTools](https://www.nuget.org/packages/Syroot.CliTools) NuGet package.

## Documentation

A feature tour aswell as complete API documentation is available on [Syroot Docs](https://docs.syroot.com/clitools).
