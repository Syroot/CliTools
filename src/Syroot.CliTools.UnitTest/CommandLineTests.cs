using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Syroot.CliTools.UnitTest
{
    [TestClass]
    public class CommandLineTests
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private static readonly string[] _args = new[]
        {
            "\"C:\\Program Files (x86)\\SomeCompany\\SomeProgram.exe\"",
            "1337",
            "fopme",
            "/requiredpath", "\"D:\\Games\\A Funny Game\"",
            "-PORTNUMBER", "44494",
            "-DumpFiles",
            "/MaxUsers", "15",
            "\"Something To Ignore\"",
            "IgnoreMeAswell",
            "/renameme", "\"rename was good\"",
            "/CantSet", "12345"
        };

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        [TestMethod]
        public void GetHelpString()
        {
            HelpStringCommandLine commandLine = new HelpStringCommandLine();
            Debug.Write(commandLine.GetHelpString());
        }

        [TestMethod]
        public void EmptyCommandLine()
        {
            TestCommandLine testCommandLine = new TestCommandLine();
            Assert.ThrowsException<CommandLineException>(() => testCommandLine.Parse(new string[0]));
        }

        [TestMethod]
        public void MissingRequiredArguments()
        {
            TestCommandLine testCommandLine = new TestCommandLine();
            Assert.ThrowsException<CommandLineException>(() => testCommandLine.Parse(_args.Skip(8).ToArray()));
        }

        [TestMethod]
        public void NoProgramCommandLine()
        {
            NoProgNameCommandLine noProgNameCommandLine = new NoProgNameCommandLine();
            noProgNameCommandLine.Parse(_args.Skip(1).ToArray());
            Assert.AreEqual(1337, noProgNameCommandLine.PositionalInt32);
            Assert.AreEqual("fopme", noProgNameCommandLine.PositionalString);
            Assert.AreEqual(33, noProgNameCommandLine.CantSet);
            Assert.AreEqual(@"D:\Games\A Funny Game", noProgNameCommandLine.RequiredPath);
            Assert.AreEqual(44494, noProgNameCommandLine.PortNumber);
            Assert.AreEqual(true, noProgNameCommandLine.DumpFiles);
            Assert.AreEqual("rename was good", noProgNameCommandLine.RenamedArg);
            Assert.AreEqual(default, noProgNameCommandLine.MissingSetter);
            Assert.AreEqual(default, noProgNameCommandLine.PrivateSetter);
        }

        [TestMethod]
        public void ProgramCommandLine()
        {
            TestCommandLine testCommandLine = new TestCommandLine();
            testCommandLine.Parse(_args, true);
            Assert.AreEqual(@"C:\Program Files (x86)\SomeCompany\SomeProgram.exe", testCommandLine.ProgramName);
            Assert.AreEqual(1337, testCommandLine.PositionalInt32);
            Assert.AreEqual("fopme", testCommandLine.PositionalString);
            Assert.AreEqual(33, testCommandLine.CantSet);
            Assert.AreEqual(@"D:\Games\A Funny Game", testCommandLine.RequiredPath);
            Assert.AreEqual(44494, testCommandLine.PortNumber);
            Assert.AreEqual("rename was good", testCommandLine.RenamedArg);
            Assert.AreEqual(true, testCommandLine.DumpFiles);
            Assert.AreEqual(default, testCommandLine.MissingSetter);
            Assert.AreEqual(default, testCommandLine.PrivateSetter);
        }

        [TestMethod]
        public void DetectBool()
        {
            BoolCommandLine boolCommandLine = new BoolCommandLine();
            boolCommandLine.Parse("\"C:\\Some\\Path\" /fast");
            Assert.AreEqual(@"C:\Some\Path", boolCommandLine.Path);
            Assert.AreEqual(true, boolCommandLine.Fast);
        }
    }

    internal class HelpStringCommandLine : CommandLine
    {
        [CommandLineArg(Description = "Sets the server port under which it can be reached.")]
        internal ushort Port = 1000;

        [CommandLineArg(Description = "If provided, UDP communication is done rather than TCP.")]
        internal bool UseUdp = false;
    }

    internal class NoProgNameCommandLine : CommandLine
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        internal readonly int CantSet = 33;

        [CommandLineArg(Required = true)]
        internal string RequiredPath = "DefaultPath";

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [CommandLineArg(1)]
        internal int PositionalInt32 { get; set; }

        [CommandLineArg(2)]
        internal string PositionalString { get; set; }

        [CommandLineArg(Required = true)]
        internal ushort PortNumber { get; set; }

        [CommandLineArg(Name = "RenameMe")]
        internal string RenamedArg { get; set; }

        internal bool DumpFiles { get; set; }

        internal int MissingSetter { get; }

        internal int PrivateSetter { get; private set; }
    }

    internal class TestCommandLine : CommandLine
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        internal readonly int CantSet = 33;

        [CommandLineArg(Required = true, Description = "This is a required path.")]
        internal string RequiredPath = "DefaultPath";

#pragma warning disable 0169 // Ignore _privateString never being used.
        private string _privateString;
#pragma warning restore 0169

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        [CommandLineArg(1, Description = "This parameter can be set as first.")]
        internal int PositionalInt32 { get; set; }

        [CommandLineArg(2, Description = "This parameter can be set as second.")]
        internal string PositionalString { get; set; }

        [CommandLineArg(Required = true, Description = "Sets a funny port number for some server.")]
        internal ushort PortNumber { get; set; }

        [CommandLineArg(Name = "RenameMe", Description = "I was renamed to map the argument somewhere else.")]
        internal string RenamedArg { get; set; }

        [CommandLineArg(Description = "This is a nice boolean argument.")]
        internal bool DumpFiles { get; set; }

        internal int MissingSetter { get; }

        internal int PrivateSetter { get; private set; }
    }

    internal class BoolCommandLine : CommandLine
    {
        [CommandLineArg(Index = 1, Required = true, Description = "Specifies the path to hell.")]
        public string Path { get; set; } = String.Empty;

        [CommandLineArg(Description = "Does things faster than usual.")]
        public bool Fast { get; set; }
    }
}