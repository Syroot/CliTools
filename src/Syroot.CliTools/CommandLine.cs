﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Syroot.CliTools
{
    /// <summary>
    /// Represents values parsed from command line arguments.
    /// </summary>
    public abstract class CommandLine
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the path to the executable file if it was passed as the first argument.
        /// </summary>
        [CommandLineArg(0)]
        public string ProgramName { get; internal set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Formats a help message from the arguments of this command line.
        /// </summary>
        /// <param name="description">An optional description displayed in the first line.</param>
        /// <returns>A string usable as the help message of this command line.</returns>
        public string GetHelpString(string description = null)
        {
            CommandLineData data = CommandLineData.Get(GetType());
            StringBuilder help = new StringBuilder();

            int longestArg = 0;
            List<CommandLineArg> args = new List<CommandLineArg>();

            // Add the descriptive title.
            if (description != null)
                help.AppendLine(description);
            help.AppendLine();

            // Add the parameter line.
            string programName = Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().Location).ToUpper();
            help.Append(programName);
            foreach (CommandLineArg arg in data.IndexedArgs.Values)
            {
                // Ignore the program name parameter.
                if (arg.Index == 0)
                    continue;

                args.Add(arg);
                longestArg = Math.Max(longestArg, arg.Name.Length);

                help.Append("  ");
                if (arg.Required)
                    help.Append($"{arg.Name}");
                else
                    help.Append($"[{arg.Name}]");
            }
            foreach (CommandLineArg arg in data.NamedArgs.Values)
            {
                // Ignore the program name parameter.
                if (arg.Name == nameof(ProgramName))
                    continue;

                args.Add(arg);
                longestArg = Math.Max(longestArg, arg.Name.Length);

                help.Append("  ");
                string valueText = arg.MemberType == typeof(Boolean) ? String.Empty : " value";
                if (arg.Required)
                    help.Append($"/{arg.Name}{valueText}");
                else
                    help.Append($"[/{arg.Name}{valueText}]");
            }
            help.AppendLine();

            // Add the parameter descriptions.
            help.AppendLine();
            foreach (CommandLineArg arg in args)
            {
                if (arg.Description != null)
                {
                    help.Append('\t');
                    help.Append(arg.Name.PadRight(longestArg + 2));
                    help.AppendLine(arg.Description);
                }
            }

            return help.ToString();
        }

        /// <summary>
        /// Parses arguments retrieved through the <see cref="Environment.GetCommandLineArgs()"/> method.
        /// </summary>
        public void Parse() => Parse(Environment.GetCommandLineArgs(), true);

        /// <summary>
        /// Parses the given command line <paramref name="text"/> and optionally ignores the first argument.
        /// </summary>
        /// <param name="text">The command line to parse.</param>
        /// <param name="hasProgramNameArg"><see langword="true"/> to handle the first argument as the
        /// <see cref="ProgramName"/>.</param>
        public void Parse(string text, bool hasProgramNameArg = false)
        {
            char[] chars = text.ToCharArray();
            bool inQuote = false;
            for (int index = 0; index < chars.Length; index++)
            {
                if (chars[index] == '"')
                    inQuote = !inQuote;
                else if (!inQuote && chars[index] == ' ')
                    chars[index] = '\n';
            }
            Parse(new string(chars).Split('\n'), hasProgramNameArg);
        }

        /// <summary>
        /// Parses the given <paramref name="args"/> and optionally ignores the first argument.
        /// </summary>
        /// <param name="args">The command line arguments to parse.</param>
        /// <param name="hasProgramNameArg"><see langword="true"/> to handle the first array element as the
        /// <see cref="ProgramName"/>.</param>
        public virtual void Parse(IList<string> args, bool hasProgramNameArg = false)
        {
            // Parse the command line and extract the values.
            CommandLineData data = CommandLineData.Get(GetType());

            // Collect a list of required arguments to check if they have been satisfied.
            List<CommandLineArg> requiredArgs = new List<CommandLineArg>(data.RequiredArgs);

            int currentIndex = hasProgramNameArg ? 0 : 1;
            for (int i = 0; i < args.Count; i++)
            {
                string arg = args[i];
                if ((arg.StartsWith("/") || arg.StartsWith("-"))
                    && data.NamedArgs.TryGetValue(arg.Substring(1), out CommandLineArg namedArg))
                {
                    // This is a named argument. Get the following value if required. If it is missing, ignore the arg.
                    if (namedArg.MemberType == typeof(Boolean))
                    {
                        namedArg.SetValue(this, true);
                        requiredArgs.Remove(namedArg);
                    }
                    else if (i < args.Count - 1)
                    {
                        namedArg.SetParsedValue(this, args[++i]);
                        requiredArgs.Remove(namedArg);
                    }
                }
                else if (data.IndexedArgs.TryGetValue(currentIndex++, out CommandLineArg indexedArg))
                {
                    // This is a positional argument. Set the value at this index directly.
                    indexedArg.SetParsedValue(this, arg);
                    requiredArgs.Remove(indexedArg);
                }
            }

            // Check if all required args were satisfied.
            if (requiredArgs.Count > 0)
                throw new CommandLineException("Some required arguments have not been provided.");
        }
    }
}
