﻿using System;
using System.Reflection;

namespace Syroot.CliTools
{
    /// <summary>
    /// Represents the member of a class which can be assigned from a command line argument.
    /// </summary>
    internal class CommandLineArg
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private MemberInfo _memberInfo;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal CommandLineArg(FieldInfo fieldInfo)
            : this(fieldInfo, fieldInfo.FieldType)
        {
            // Ignore private and readonly fields.
            IsSupported = IsSupported
                && !fieldInfo.IsPrivate && !fieldInfo.IsInitOnly;
        }

        internal CommandLineArg(PropertyInfo propertyInfo)
            : this(propertyInfo, propertyInfo.PropertyType)
        {
            // Ignore private properties with private a getter and setter and those not having both.
            IsSupported = IsSupported
                && propertyInfo.GetMethod?.IsPrivate == false && propertyInfo.SetMethod?.IsPrivate == false;
        }

        private CommandLineArg(MemberInfo memberInfo, Type memberType)
        {
            _memberInfo = memberInfo;
            MemberType = memberType;
            Name = memberInfo.Name;
            IsSupported = IsSupportedType(MemberType);

            // Store attribute configuration.
            CommandLineArgAttribute attrib = memberInfo.GetCustomAttribute<CommandLineArgAttribute>();
            if (attrib != null)
            {
                Index = attrib.Index;
                Name = attrib.Name ?? Name;
                Description = attrib.Description;
                Required = attrib.Required;
            }
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal Type MemberType { get; }

        internal bool IsSupported { get; }

        internal int Index { get; } = Int32.MinValue;

        internal string Name { get; }

        internal string Description { get; }

        internal bool Required { get; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void SetValue(object instance, object value)
        {
            switch (_memberInfo)
            {
                case FieldInfo fieldInfo:
                    fieldInfo.SetValue(instance, value);
                    break;
                case PropertyInfo propertyInfo:
                    propertyInfo.SetValue(instance, value);
                    break;
                default:
                    throw new NotImplementedException("Cannot set unknown MemberInfo type.");
            }
        }

        internal bool SetParsedValue(object instance, string value)
        {
            if (TryParseValue(value, out object parsedValue))
            {
                SetValue(instance, parsedValue);
                return true;
            }
            return false;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static bool IsSupportedType(Type type)
            => type == typeof(String)
            || type == typeof(Boolean)
            || type == typeof(Byte)
            || type == typeof(Decimal)
            || type == typeof(Double)
            || type == typeof(Int16)
            || type == typeof(Int32)
            || type == typeof(Int64)
            || type == typeof(SByte)
            || type == typeof(Single)
            || type == typeof(UInt16)
            || type == typeof(UInt32)
            || type == typeof(UInt64);

        private bool TryParseValue(string value, out object parsedValue)
        {
            bool result = false;

            if (MemberType == typeof(String))
            {
                // Remove one pair of quotes from start and end.
                if (value[0] == '"' && value[value.Length - 1] == '"')
                    parsedValue = value.Substring(1, value.Length - 2);
                else
                    parsedValue = value;
                result = true;
            }
            else if (MemberType == typeof(Byte) && (result = Byte.TryParse(value, out Byte parsedByte)))
                parsedValue = parsedByte;
            else if (MemberType == typeof(Decimal) && (result = Decimal.TryParse(value, out Decimal parsedDecimal)))
                parsedValue = parsedDecimal;
            else if (MemberType == typeof(Double) && (result = Double.TryParse(value, out Double parsedDouble)))
                parsedValue = parsedDouble;
            else if (MemberType == typeof(Int16) && (result = Int16.TryParse(value, out Int16 parsedInt16)))
                parsedValue = parsedInt16;
            else if (MemberType == typeof(Int32) && (result = Int32.TryParse(value, out Int32 parsedInt32)))
                parsedValue = parsedInt32;
            else if (MemberType == typeof(Int64) && (result = Int64.TryParse(value, out Int64 parsedInt64)))
                parsedValue = parsedInt64;
            else if (MemberType == typeof(SByte) && (result = SByte.TryParse(value, out SByte parsedSByte)))
                parsedValue = parsedSByte;
            else if (MemberType == typeof(Single) && (result = Single.TryParse(value, out Single parsedSingle)))
                parsedValue = parsedSingle;
            else if (MemberType == typeof(UInt16) && (result = UInt16.TryParse(value, out UInt16 parsedUInt16)))
                parsedValue = parsedUInt16;
            else if (MemberType == typeof(UInt32) && (result = UInt32.TryParse(value, out UInt32 parsedUInt32)))
                parsedValue = parsedUInt32;
            else if (MemberType == typeof(UInt64) && (result = UInt64.TryParse(value, out UInt64 parsedUInt64)))
                parsedValue = parsedUInt64;
            else
                parsedValue = null;

            return result;
        }
    }
}
