﻿using System;

namespace Syroot.CliTools
{
    /// <summary>
    /// Represents a command line argument configuration for use when parsing a <see cref="CommandLine"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class CommandLineArgAttribute : Attribute
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandLineArgAttribute"/> class.
        /// </summary>
        public CommandLineArgAttribute() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandLineArgAttribute"/> class appearing at the given
        /// <paramref name="index"/>.
        /// </summary>
        /// <param name="index">The index at which the argument appears. Must be equal to or bigger than 1 as the
        /// argument at index 0 is reserved for the program name.</param>
        public CommandLineArgAttribute(int index)
        {
            Index = index;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the index at which the argument appears. Must be equal to or bigger than 1 as the argument at
        /// index 0 is reserved for the program name.
        /// </summary>
        public int Index { get; set; } = -1;

        /// <summary>
        /// Gets or sets the name with which this argument can be assigned. Required in case obfuscation modifies member
        /// names.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a help description of the argument.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this command line argument is required to be specified or optional.
        /// </summary>
        public bool Required { get; set; }
    }
}
