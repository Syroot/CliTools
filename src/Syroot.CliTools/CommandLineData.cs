﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Syroot.CliTools
{
    /// <summary>
    /// Represents a cache of reflection information for <see cref="CommandLine"/> classes.
    /// </summary>
    internal class CommandLineData
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static Dictionary<Type, CommandLineData> _cache = new Dictionary<Type, CommandLineData>();

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        private CommandLineData(Type type)
        {
            NamedArgs = new SortedDictionary<string, CommandLineArg>(StringComparer.OrdinalIgnoreCase);
            IndexedArgs = new SortedDictionary<int, CommandLineArg>();
            RequiredArgs = new List<CommandLineArg>();

            // Find all fields properties to fill in from the command line.
            foreach (MemberInfo memberInfo in type.GetMembers(
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy))
            {
                CommandLineArg argument = null;
                switch (memberInfo)
                {
                    case FieldInfo fieldInfo:
                        argument = new CommandLineArg(fieldInfo);
                        break;
                    case PropertyInfo propertyInfo:
                        argument = new CommandLineArg(propertyInfo);
                        break;
                }
                if (argument?.IsSupported == true)
                {
                    if (argument.Index > -1)
                        IndexedArgs.Add(argument.Index, argument);
                    else
                        NamedArgs.Add(argument.Name, argument);
                    if (argument.Required)
                        RequiredArgs.Add(argument);
                }
            }
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal IDictionary<string, CommandLineArg> NamedArgs { get; }

        internal IDictionary<int, CommandLineArg> IndexedArgs { get; }

        internal IList<CommandLineArg> RequiredArgs { get; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static CommandLineData Get(Type type)
        {
            lock (_cache)
            {
                if (!_cache.TryGetValue(type, out CommandLineData data))
                {
                    data = new CommandLineData(type);
                    _cache.Add(type, data);
                }
                return data;
            }
        }
    }
}
