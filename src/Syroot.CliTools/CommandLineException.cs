﻿using System;

namespace Syroot.CliTools
{
    /// <summary>
    /// Represents the <see cref="Exception"/> thrown when an unrecoverable error with <see cref="CommandLine"/> has
    /// occured.
    /// </summary>
    public class CommandLineException : Exception
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandLineException"/> class with the given
        /// <paramref name="message"/>.
        /// </summary>
        /// <param name="message">The message to display.</param>
        public CommandLineException(string message) : base(message) { }
    }
}